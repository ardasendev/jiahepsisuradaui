import { NgModule } from '@angular/core';
import { ModelModule } from '../model/model.module';
import {FormsModule} from '@angular/forms';
import {BrowserModule} from '@angular/platform-browser';
import {MainComponent} from './main/main.component';
@NgModule({
    imports:[ModelModule, BrowserModule, FormsModule],
    providers:[],
    declarations: [
      MainComponent
    ],
    exports:[MainComponent]

})

export class PagesModule{

}