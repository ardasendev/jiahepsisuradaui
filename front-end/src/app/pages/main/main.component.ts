import { Component, OnInit } from '@angular/core';
import { Category } from 'src/app/model/category.model';
import { CategoryRepository } from 'src/app/model/category.repository';
import { Product } from 'src/app/model/product.model';
import { ProductRepository } from 'src/app/model/product.repository';

@Component({
  selector: 'main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  constructor(
    private productRepository: ProductRepository,
    private categoryRepository: CategoryRepository
  ) { }
    
  ngOnInit(): void {
  }
  get products():Product[]{
    return this.productRepository.getProducts();
  }
  get categories():Category[]{
    return this.categoryRepository.getCategories();
  }

}
