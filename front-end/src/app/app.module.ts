import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './pages/header/header.component';
import { FooterComponent } from './pages/footer/footer.component';
import { MainComponent } from './pages/main/main.component';
import { SearchComponent } from './pages/header/search/search.component';
import { NavbarComponent } from './pages/header/navbar/navbar.component';
import { SliderComponent } from './pages/main/slider/slider.component';
import { ProductListComponent } from './pages/main/product-list/product-list.component';
import { ItemComponentComponent } from './pages/main/product-list/item-component/item-component.component';
import { LoginComponent } from './pages/main/login/login.component';
import { ReactiveFormsModule } from '@angular/forms';
import { LoginServiceService } from './pages/services/login-service.service';
import { HttpClientModule } from '@angular/common/http';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    MainComponent,
    SearchComponent,
    NavbarComponent,
    SliderComponent,
    ProductListComponent,
    ItemComponentComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [
    LoginServiceService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
