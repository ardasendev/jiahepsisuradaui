import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './pages/main/login/login.component';
import { MainComponent } from './pages/main/main.component';

const routes: Routes = [
  {
    path:'', component: MainComponent
  },
  {
    path:'main', component: MainComponent
  },
  {
    path:'login', component : LoginComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
